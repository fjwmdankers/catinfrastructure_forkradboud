set "currentFolder=%~dp0"
set "d2rLocation=C:\JavaPrograms\d2rq-0.8.1"
set "rdfPushToolLocation=C:\JavaPrograms\RDF_file_pusher"
set "endpointLocation=http://localhost:81/openrdf-sesame/repositories/cat"
set "rdfGraph=http://localhost/graphs/mosaiq"

c:
cd %d2rLocation%

CALL dump-rdf.bat --verbose -o %currentFolder%output.ttl %currentFolder%%1

cd %rdfPushToolLocation%

set "clean="

if not "%2"=="" ( set "clean=-cg true" )

java -jar RdfFilePusher-1.9.0-SNAPSHOT-jar-with-dependencies.jar -f %currentFolder%output.ttl -e %endpointLocation% -g %rdfGraph% %clean%

d:
cd %currentFolder%

del output.ttl